////////////////////////////////////////////////////////////////////////
// Frame Screen Saver.
// Copyright (C) 2002. Hyoungsoo Yoon.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@computer.org
////////////////////////////////////////////////////////////////////////
//
// PhotoWnd.cpp : implementation file
//

#include "stdafx.h"
#include "Frame.h"
#include "PhotoWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CPhotoWnd

CPhotoWnd::CPhotoWnd()
{
}

CPhotoWnd::~CPhotoWnd()
{
}


BEGIN_MESSAGE_MAP(CPhotoWnd, CScreenWnd)
	//{{AFX_MSG_MAP(CPhotoWnd)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CPhotoWnd message handlers


BOOL CPhotoWnd::Create()
{

    if (!CScreenWnd::Create()) {
        return FALSE;
    }


    return TRUE;
}


BOOL CPhotoWnd::Create(BOOL bPreview, const RECT& rect, CWnd* pParentWnd)
{
    if (!CScreenWnd::Create(bPreview, rect, pParentWnd)) 
    {
        return FALSE;
    }
    return TRUE;
}
