////////////////////////////////////////////////////////////////////////
// Frame Screen Saver.
// Copyright (C) 2002. Hyoungsoo Yoon.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@computer.org
////////////////////////////////////////////////////////////////////////
//
// Frame.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Frame.h"
#include "ConfigDlg.h"
#include "PhotoWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CFrameApp

BEGIN_MESSAGE_MAP(CFrameApp, CWinApp)
	//{{AFX_MSG_MAP(CFrameApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFrameApp construction

CFrameApp::CFrameApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CFrameApp object

CFrameApp theApp;


/////////////////////////////////////////////////////////////////////////////
// Convenience function to parse command line options.

BOOL MatchOption(LPTSTR lpsz, LPTSTR lpszOption)
{
	if (lpsz[0] == '-' || lpsz[0] == '/')
		lpsz++;
	if (lstrcmpi(lpsz, lpszOption) == 0)
		return TRUE;
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CFrameApp initialization

BOOL CFrameApp::InitInstance()
{
    // If the command line contains /s -s or s then just run as a 
    // screen saver.  If the command line is blank or contains 
    // /c -c or c then show the config dialog

	Enable3dControls();
	SetRegistryKey(s_profileCompany);

    ////////////////////////////////////////////////////////////
    // This section is what makes screen savers screen savers!
    ////////////////////////////////////////////////////////////
    if (__argc == 1)
    {
        // Run the config dlg with no parent
        DoConfig();
    }
	else if (MatchOption(__argv[1], _T("s")))
	{
        // Run as screen saver
        CPhotoWnd* pWnd = new CPhotoWnd();
        pWnd->Create();
        m_pMainWnd = pWnd;
	    return TRUE;
	}
	else if (MatchOption(__argv[1], _T("p")))
	{
        // Run as a subwindow...
		CWnd* pParent = CWnd::FromHandle((HWND)atol(__argv[2]));
		ASSERT(pParent != NULL);
		CPhotoWnd* pWnd = new CPhotoWnd();
		CRect rect;
		pParent->GetClientRect(&rect);
        pWnd->Create(TRUE, rect, pParent);
		m_pMainWnd = pWnd;
		return TRUE;
	}
    else 
    {
        // Run the configuration dialog
        if (MatchOption(__argv[1], _T("c")))
        {
            // Run config with current window as parent
            DoConfig(CWnd::GetActiveWindow());
        } else {
            // Run the config dlg with no parent
            DoConfig();
        }
    }
    ////////////////////////////////////////////////////////////
    // This section is what makes screen savers screen savers!
    ////////////////////////////////////////////////////////////

    // Now just terminate
    return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// Open the "Settings..." window.

void CFrameApp::DoConfig(CWnd* pParent)
{
    CConfigDlg dlg(pParent);

    // Set up the current params
	dlg.m_nSpeed = GetProfileInt(s_profileConfig,s_profileConfigAnimationSpeed, 80);
	dlg.m_colorLogo = (COLORREF) GetProfileInt(s_profileConfig, s_profileConfigLogoColor, 0xFFFFFF);
	dlg.m_bUseBWLogo = (BOOL) GetProfileInt(s_profileConfig, s_profileConfigLogoBWLogo, 1);
	dlg.m_nLogoSizeX = GetProfileInt(s_profileConfig, s_profileConfigLogoWidth, 316);
	dlg.m_nLogoSizeY = GetProfileInt(s_profileConfig, s_profileConfigLogoHeight, 190);
    //dlg.m_nBackgroundMode = (BackgroundMode) GetProfileInt(s_profileConfig, _T("Background Mode"), (int) BG_BLACK);
    dlg.m_nBackground = GetProfileInt(s_profileConfig, s_profileConfigBackgroundMode, (int) BG_BLACK);
    
    
    m_pMainWnd = &dlg;

    // Do the dialog
    if (dlg.DoModal() == IDOK)
    {
		WriteProfileInt(s_profileConfig,s_profileConfigAnimationSpeed, dlg.m_nSpeed);
		WriteProfileInt(s_profileConfig, s_profileConfigLogoColor, (int) dlg.m_colorLogo);
		WriteProfileInt(s_profileConfig, s_profileConfigLogoBWLogo, (int) dlg.m_bUseBWLogo);
		WriteProfileInt(s_profileConfig, s_profileConfigLogoWidth, dlg.m_nLogoSizeX);
		WriteProfileInt(s_profileConfig, s_profileConfigLogoHeight, dlg.m_nLogoSizeY);
        //WriteProfileInt(s_profileConfig, _T("Background Mode"), (int) dlg.m_nBackgroundMode);
        WriteProfileInt(s_profileConfig, s_profileConfigBackgroundMode, (int) dlg.m_nBackground);
	}

}


/////////////////////////////////////////////////////////////////////////////
// CFrameApp message handlers


