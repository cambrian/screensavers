; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CConfigDlg
LastTemplate=generic CWnd
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "BrioLogo.h"
LastPage=0

ClassCount=8
Class1=CBrioLogoApp
Class3=CMainFrame
Class4=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Class2=CChildView
Class5=CLogoWnd
Class6=CScrnWnd
Resource2=IDD_CONFIG_DIALOG
Class7=CConfigDlg
Class8=CSaverWnd
Resource3=IDR_MAINFRAME

[CLS:CBrioLogoApp]
Type=0
HeaderFile=BrioLogo.h
ImplementationFile=BrioLogo.cpp
Filter=N
LastObject=CBrioLogoApp
BaseClass=CWinApp
VirtualFilter=AC

[CLS:CChildView]
Type=0
HeaderFile=ChildView.h
ImplementationFile=ChildView.cpp
Filter=N

[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T




[CLS:CAboutDlg]
Type=0
HeaderFile=BrioLogo.cpp
ImplementationFile=BrioLogo.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=5
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=IDC_STATIC,static,1342308352

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_APP_EXIT
Command2=ID_EDIT_UNDO
Command3=ID_EDIT_CUT
Command4=ID_EDIT_COPY
Command5=ID_EDIT_PASTE
Command6=ID_APP_ABOUT
CommandCount=6

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_EDIT_COPY
Command2=ID_EDIT_PASTE
Command3=ID_EDIT_UNDO
Command4=ID_EDIT_CUT
Command5=ID_NEXT_PANE
Command6=ID_PREV_PANE
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_CUT
Command10=ID_EDIT_UNDO
CommandCount=10

[CLS:CLogoWnd]
Type=0
HeaderFile=LogoWnd.h
ImplementationFile=LogoWnd.cpp
BaseClass=generic CWnd
Filter=W

[CLS:CScrnWnd]
Type=0
HeaderFile=ScrnWnd.h
ImplementationFile=ScrnWnd.cpp
BaseClass=CSaverWnd
Filter=W
VirtualFilter=WC
LastObject=CScrnWnd

[DLG:IDD_CONFIG_DIALOG]
Type=1
Class=CConfigDlg
ControlCount=20
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_ABOUT,button,1342242816
Control4=IDC_STATIC,button,1342308359
Control5=IDC_BACK_BLACK,button,1342373897
Control6=IDC_BACK_SCREEN,button,1342242825
Control7=IDC_STATIC,button,1342177287
Control8=IDC_STATIC,static,1342308352
Control9=IDC_LOGO_SIZEX,edit,1350631552
Control10=IDC_STATIC,static,1342308352
Control11=IDC_LOGO_SIZEY,edit,1350631552
Control12=IDC_STATIC,static,1342308352
Control13=IDC_LOGO_COLOR_PREVIEW,static,1342308352
Control14=IDC_LOGO_COLOR_CHANGE,button,1342242816
Control15=IDC_STATIC,button,1342177287
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_SPEED_BAR,scrollbar,1342177280
Control19=IDC_PREVIEW,static,1073741824
Control20=IDC_USE_BWLOGO,button,1342242819

[CLS:CConfigDlg]
Type=0
HeaderFile=ConfigDlg.h
ImplementationFile=ConfigDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CConfigDlg
VirtualFilter=dWC

[CLS:CSaverWnd]
Type=0
HeaderFile=SaverWnd.h
ImplementationFile=SaverWnd.cpp
BaseClass=CWnd
Filter=W
VirtualFilter=WC
LastObject=CSaverWnd

