[Frame Screen Saver]

CFrameApp: Main Driver class
CSaverWnd: This class implements essential functions of Screen saver.
CScreenWnd: This class provides basic drawing routines imitating "Windows logon screen saver".
CPhotoWnd: Simple subclass of CScreenWnd. (Currently, this class doesn't introduce any new functionalities over CScreenWnd.)
CConfigDlg: Dialog window to be used for "Setting..." purporses.


