////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LogoInfo.h"


CLogoInfo::CLogoInfo(CString name, UINT imageID, int originalSizeX, int originalSizeY, int optimalSizeX, int optimalSizeY)
    : m_strName(name), m_nImageID(imageID), m_nOriginalSizeX(originalSizeX), m_nOriginalSizeY(originalSizeY),
    m_nOptimalSizeX(optimalSizeX > 0 ? optimalSizeX : 2*originalSizeX),  // Just use the size twice the original as "optimal" one by default.
    m_nOptimalSizeY(optimalSizeY > 0 ? optimalSizeY : 2*originalSizeY)
{
}

CLogoInfo::~CLogoInfo()
{
}

// Returns the unique name of this logo.
CString  CLogoInfo::GetName()
{
    return m_strName;
}

// Returns the first of the three consecutive Image IDs.
UINT     CLogoInfo::GetImageID()
{
    return m_nImageID;
}

// Returns the original width of the bitmap image. 
int      CLogoInfo::GetOriginalSizeX()
{
    return m_nOriginalSizeX;
}

// Returns the original height of the bitmap image. 
int      CLogoInfo::GetOriginalSizeY()
{
    return m_nOriginalSizeY;
}

// Returns the "recommended" width of the image. 
int      CLogoInfo::GetOptimalSizeX()
{
    return m_nOptimalSizeX;
}

// Returns the "recommended" height of the image. 
int      CLogoInfo::GetOptimalSizeY()
{
    return m_nOptimalSizeY;
}

