////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////

#pragma once


// Simple struct to collect some logo-related attributes.
class CLogoInfo
{
public:
    CLogoInfo(CString name, UINT imageID, int originalSizeX, int originalSizeY, int optimalSizeX = 0, int optimalSizeY = 0);
    ~CLogoInfo();

public:
    CString  GetName();
    UINT     GetImageID();
    int      GetOriginalSizeX();
    int      GetOriginalSizeY();
    int      GetOptimalSizeX();
    int      GetOptimalSizeY();

private:
    // Unique name.
    CString  m_strName;         

    // TODO: We use THREE images per logo!!!
    // Temporarily, we assume that the IDs of the three images are consecutive.
    // E.g. GRAY = NORMAL + 1 and REVERSE = GRAY + 1.

    // Image resource ID for the "NORMAL" image.
    UINT     m_nImageID;

    int      m_nOriginalSizeX;  // in pixels
    int      m_nOriginalSizeY;  // in pixels
    int      m_nOptimalSizeX;   // in pixels
    int      m_nOptimalSizeY;   // in pixels
};
