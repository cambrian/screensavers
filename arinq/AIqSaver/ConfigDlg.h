////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////
//
// ConfigDlg.h : header file
//
#if !defined(AFX_CONFIGDLG_H__668F7D25_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
#define AFX_CONFIGDLG_H__668F7D25_D6DF_11D5_8DA8_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LogoWnd.h"


/////////////////////////////////////////////////////////////////////////////
// CConfigDlg dialog

class CConfigDlg : public CDialog
{
// Construction
public:
	CConfigDlg(CWnd* pParent = NULL);   // standard constructor

public:
// Dialog Data
	//{{AFX_DATA(CConfigDlg)
	enum { IDD = IDD_CONFIG_DIALOG };
	CScrollBar	m_scrollSpeed;
	int		m_nLogoSizeX;
	int		m_nLogoSizeY;
	int		m_nSpeed;
	int		m_nBackground;
	BOOL	m_bUseBWLogo;
	//}}AFX_DATA

public:
    //BackgroundMode  m_nBackgroundMode;
    CString         m_strLogoImageName;
    BOOL            m_bUseDefaultAspectRatio;

private:
    int             m_nOptimalLogoWidth;
    int             m_nOptimalLogoHeight;

protected:
    HICON           m_hIcon;
    CLogoWnd        m_wndPreview;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CConfigDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnBackBlack();
	afx_msg void OnBackScreen();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
    afx_msg void OnBnClickedUseDefratio();
    afx_msg void OnBnClickedResetSize();
    afx_msg void OnCbnSelchangeLogoImage();
    afx_msg void OnEnChangeLogoSizex();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIGDLG_H__668F7D25_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
