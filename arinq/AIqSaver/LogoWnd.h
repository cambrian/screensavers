////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////
//
// LogoWnd.h : header file
//
#if !defined(AFX_LOGOWND_H__668F7D23_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
#define AFX_LOGOWND_H__668F7D23_D6DF_11D5_8DA8_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ScrnWnd.h"


/////////////////////////////////////////////////////////////////////////////
// CLogoWnd window

class CLogoWnd : public CScrnWnd
{
// Construction
public:
	CLogoWnd();
    BOOL Create();
    BOOL Create(BOOL bPreview, const RECT& rect, CWnd* pParentWnd);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogoWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLogoWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CLogoWnd)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGOWND_H__668F7D23_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
