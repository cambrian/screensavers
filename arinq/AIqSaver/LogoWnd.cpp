////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////
//
// LogoWnd.cpp : implementation file
//

#include "stdafx.h"
#include "AIqSaver.h"
#include "LogoWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CLogoWnd

CLogoWnd::CLogoWnd() : CScrnWnd()
{
}

CLogoWnd::~CLogoWnd()
{
}


BEGIN_MESSAGE_MAP(CLogoWnd, CScrnWnd)
	//{{AFX_MSG_MAP(CLogoWnd)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CLogoWnd message handlers


BOOL CLogoWnd::Create()
{
    if (!CScrnWnd::Create()) {
        return FALSE;
    }

    return TRUE;
}


BOOL CLogoWnd::Create(BOOL bPreview, const RECT& rect, CWnd* pParentWnd)
{
    if (!CScrnWnd::Create(bPreview, rect, pParentWnd)) 
    {
        return FALSE;
    }
    return TRUE;
}

