////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////
//
// ConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AIqSaver.h"
#include "AboutDlg.h"
#include "ConfigDlg.h"
#include <map>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CConfigDlg dialog

CConfigDlg::CConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDlg::IDD, pParent), m_wndPreview(), m_bUseDefaultAspectRatio(TRUE)
{
	//{{AFX_DATA_INIT(CConfigDlg)
	m_nLogoSizeX = 0;
	m_nLogoSizeY = 0;
	m_nSpeed = 0;
	m_nBackground = -1;
	m_bUseBWLogo = FALSE;
	//}}AFX_DATA_INIT

    //m_nBackgroundMode = BG_BLACK;	
    m_strLogoImageName = CString(_T("Default Logo"));

   	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}


void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigDlg)
	DDX_Control(pDX, IDC_SPEED_BAR, m_scrollSpeed);
	DDX_Text(pDX, IDC_LOGO_SIZEX, m_nLogoSizeX);
	DDV_MinMaxInt(pDX, m_nLogoSizeX, 16, 1600);
	DDX_Text(pDX, IDC_LOGO_SIZEY, m_nLogoSizeY);
	DDV_MinMaxInt(pDX, m_nLogoSizeY, 16, 1600);
	DDX_Scroll(pDX, IDC_SPEED_BAR, m_nSpeed);
	DDX_Radio(pDX, IDC_BACK_BLACK, m_nBackground);
	DDX_Check(pDX, IDC_USE_BWLOGO, m_bUseBWLogo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CConfigDlg)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BACK_BLACK, OnBackBlack)
	ON_BN_CLICKED(IDC_BACK_SCREEN, OnBackScreen)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_ABOUT, OnAbout)
	//}}AFX_MSG_MAP
    ON_BN_CLICKED(IDC_USE_DEFRATIO, OnBnClickedUseDefratio)
    ON_BN_CLICKED(IDC_RESET_SIZE, OnBnClickedResetSize)
    ON_CBN_SELCHANGE(IDC_LOGO_IMAGE, OnCbnSelchangeLogoImage)
//    ON_CBN_SELENDOK(IDC_LOGO_IMAGE, OnCbnSelendokLogoImage)
//ON_CBN_EDITCHANGE(IDC_LOGO_IMAGE, OnCbnEditchangeLogoImage)
ON_EN_CHANGE(IDC_LOGO_SIZEX, OnEnChangeLogoSizex)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg message handlers

BOOL CConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CRect rect;
	GetDlgItem(IDC_PREVIEW)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	m_wndPreview.Create(TRUE, rect, this);

	m_scrollSpeed.SetScrollRange(1, 100);
	m_scrollSpeed.SetScrollPos(m_nSpeed);

    CAIqSaverApp* pApp = (CAIqSaverApp*) AfxGetApp();
    CComboBox* pLogoImage = (CComboBox*) GetDlgItem(IDC_LOGO_IMAGE);

    std::map<CString, CLogoInfo>::iterator it;
    for(it = pApp->m_mapLogoList.begin(); it != pApp->m_mapLogoList.end(); ++it)
    {
        pLogoImage->AddString(it->first);
    }

    it = pApp->m_mapLogoList.find(m_strLogoImageName);
    if(it == pApp->m_mapLogoList.end())
    {
         it = pApp->m_mapLogoList.begin();  // Just use the first element...
         m_strLogoImageName = it->first;
    }
    pLogoImage->SetWindowText(m_strLogoImageName);

    m_nOptimalLogoWidth = it->second.GetOptimalSizeX();
    m_nOptimalLogoHeight = it->second.GetOptimalSizeY();

    CButton* pCheckBoxDefRatio = (CButton*) GetDlgItem(IDC_USE_DEFRATIO);
    pCheckBoxDefRatio->SetCheck(m_bUseDefaultAspectRatio);
    CEdit* pEditSizeY = (CEdit*) GetDlgItem(IDC_LOGO_SIZEY);
    pEditSizeY->EnableWindow(!m_bUseDefaultAspectRatio);


	CenterWindow();

    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

    return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CConfigDlg::OnPaint() 
{
    // This is not really necessary since the Settings/Config dialog will never be minimized.
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// TBD
void CConfigDlg::OnBackBlack() 
{
    //m_nBackgroundMode = BG_BLACK;	
    //m_nBackground = 0;
}

// TBD
void CConfigDlg::OnBackScreen() 
{
    //m_nBackgroundMode = BG_SCREEN;	
    //m_nBackground = 1;
}

// Implementation for the speed scroll bar.
void CConfigDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	ASSERT(pScrollBar != NULL);
	int nCurPos = pScrollBar->GetScrollPos();
	switch (nSBCode)
	{
	case SB_LEFT: //    Scroll to far left.
		nCurPos = 1;
		break;
	case SB_LINELEFT: //    Scroll left.
		nCurPos--;
		break;
	case SB_LINERIGHT: //    Scroll right.
		nCurPos++;
		break;
	case SB_PAGELEFT: //    Scroll one page left.
		nCurPos -= 10;
		break;
	case SB_PAGERIGHT: //    Scroll one page right.
		nCurPos += 10;
		break;
	case SB_RIGHT: //    Scroll to far right.
		nCurPos = 100;
		break;
	case SB_THUMBPOSITION: //    Scroll to absolute position. The current position is specified by the nPos parameter.
	case SB_THUMBTRACK: //    Drag scroll box to specified position. The current position is specified by the nPos parameter.
		nCurPos = nPos;
	}
	if (nCurPos < 1)
		nCurPos = 1;
	if (nCurPos > 100)
		nCurPos = 100;
	pScrollBar->SetScrollPos(nCurPos);

	m_wndPreview.SetSpeed(m_scrollSpeed.GetScrollPos());

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}


// Display the About dialog.
void CConfigDlg::OnAbout() 
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// Event handler for the checkbox button that indicates
// whether to use the default aspect ratio or not.
void CConfigDlg::OnBnClickedUseDefratio()
{
    CButton* pCheckBoxDefRatio = (CButton*) GetDlgItem(IDC_USE_DEFRATIO);
    CEdit* pEditSizeY = (CEdit*) GetDlgItem(IDC_LOGO_SIZEY);
    m_bUseDefaultAspectRatio = pCheckBoxDefRatio->GetCheck();
    pEditSizeY->EnableWindow(!m_bUseDefaultAspectRatio);
    if(m_bUseDefaultAspectRatio)
    {
        CEdit* pEditSizeX = (CEdit*) GetDlgItem(IDC_LOGO_SIZEX);
        CString strSizeX, strSizeY;
        pEditSizeX->GetWindowText(strSizeX);
        int height = atoi(strSizeX) * m_nOptimalLogoHeight / m_nOptimalLogoWidth;
        strSizeY.Format("%d", height);
        pEditSizeY->SetWindowText(strSizeY);
    }
}

// Resets the width and height of the given logo image with the "recommended" values.
void CConfigDlg::OnBnClickedResetSize()
{
    CEdit* pEditSizeX = (CEdit*) GetDlgItem(IDC_LOGO_SIZEX);
    CEdit* pEditSizeY = (CEdit*) GetDlgItem(IDC_LOGO_SIZEY);

    CString width, height;
    width.Format("%d", m_nOptimalLogoWidth);
    height.Format("%d", m_nOptimalLogoHeight);
    pEditSizeX->SetWindowText(width);
    pEditSizeY->SetWindowText(height);
}

// Event handler for the logo change.
void CConfigDlg::OnCbnSelchangeLogoImage()
{
    // [1] Save the image name
    CComboBox* pLogoImage = (CComboBox*) GetDlgItem(IDC_LOGO_IMAGE);
    pLogoImage->GetLBText(pLogoImage->GetCurSel(), m_strLogoImageName);
    //MessageBox("Image name: " + m_strLogoImageName);

    // [2] Reset the optimal size...
    CAIqSaverApp* pApp = (CAIqSaverApp*) AfxGetApp();
    std::map<CString, CLogoInfo>::iterator it = pApp->m_mapLogoList.find(m_strLogoImageName);
    if(it != pApp->m_mapLogoList.end())  // Is this checking necessary?
    {
        m_nOptimalLogoWidth = it->second.GetOptimalSizeX();
        m_nOptimalLogoHeight = it->second.GetOptimalSizeY();
    }

    // TODO:
    // The image bitmaps should be reloaded for Preview Window...
    // ...
}

// If the aspect ratio should be kept,
// then the height should be automatically adjusted when the width changes.
void CConfigDlg::OnEnChangeLogoSizex()
{
    if(m_bUseDefaultAspectRatio)
    {
        CEdit* pEditSizeX = (CEdit*) GetDlgItem(IDC_LOGO_SIZEX);
        CEdit* pEditSizeY = (CEdit*) GetDlgItem(IDC_LOGO_SIZEY);

        CString strSizeX, strSizeY;
        pEditSizeX->GetWindowText(strSizeX);
        int height = atoi(strSizeX) * m_nOptimalLogoHeight / m_nOptimalLogoWidth;
        strSizeY.Format("%d", height);
        pEditSizeY->SetWindowText(strSizeY);
    }
}

