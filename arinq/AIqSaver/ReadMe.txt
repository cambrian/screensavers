[[ArinQ-Logo Screen Saver: AIqSaver]]

[1] Main Classes

CAIqSaverApp:
    Main application class.

CSaverWnd:
    This class implements essential functions of Screen saver.

CScrnWnd:
    This class inherits from CSaverWnd,
    and it provides basic drawing routines imitating "Windows logon screen saver".

CLogoWnd:
    Simple subclass of CScrnWnd. (Currently, this class doesn't introduce any new functionalities over CScrnWnd.)

CConfigDlg:
    Dialog window to be used for "Setting..." purporses.

CLogoInfo:
    Simple struct for logo-related attributes.


[2] To Do

(1) Allow custom images to be loaded.
(2) Allow dynamically generated images based character strings (just like Marquee).
(3) Allow multiple images or logos to be displayed at the same time.
(4) Add a feature to cycle through multiple images.


