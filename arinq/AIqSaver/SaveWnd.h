////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////
//
// SaveWnd.h : header file
//

#ifndef _SAVEWND_
#define _SAVEWND_

/////////////////////////////////////////////////////////////////////////////
// CSaveWnd window

class CSaveWnd : public CWnd
{
// Construction
public:
	CSaveWnd();
    CSaveWnd(BOOL bAutoDelete);
    BOOL Create();

protected:
	BOOL m_bAutoDelete;

private:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaveWnd)
	protected:
	virtual void PostNcDestroy();
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSaveWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSaveWnd)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
#endif // _SAVEWND_
