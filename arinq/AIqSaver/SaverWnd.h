////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////
//
// SaverWnd.h : header file
//
#if !defined(AFX_SAVERWND_H__668F7D26_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
#define AFX_SAVERWND_H__668F7D26_D6DF_11D5_8DA8_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


/////////////////////////////////////////////////////////////////////////////
// CSaverWnd window
// This class defines/implements essential functions of screen savers.

class CSaverWnd : public CWnd
{
// Construction
public:
	CSaverWnd();
    BOOL Create();
    BOOL Create(BOOL bPreview, const RECT& rect, CWnd* pParentWnd);

protected:
	BOOL m_bPreview;  // Is this being used as a preview window?


// Attributes
public:
	static LPCTSTR m_lpszClassName;


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaverWnd)
	protected:
	virtual void PostNcDestroy();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSaverWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSaverWnd)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVERWND_H__668F7D26_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
