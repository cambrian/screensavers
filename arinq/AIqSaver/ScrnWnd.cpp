////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////
//
// ScrnWnd.cpp : implementation file
//

#include "stdafx.h"
#include "AIqSaver.h"
#include "ScrnWnd.h"
#include "LogoInfo.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CScrnWnd

CScrnWnd::CScrnWnd() : m_nBackgroundMode(BG_BLACK), m_strLogoImageName(_T("Default Logo")), m_bUseBWLogo(FALSE), m_bUseDefaultAspectRatio(TRUE)
{
    m_nLogoWidth = 2*PSoftLogoDefaultWidth; // ???
    m_nLogoHeight = 2*PSoftLogoDefaultHeight; // ???

    m_nLogoTimer = 0;
    m_nFadeTimer = 0;
    m_nResetTimer = 0;
    m_nLogoInterval = 1 * 1000;
    m_nFadeInterval = m_nLogoInterval / 10;  // Not used at the moment...
    m_nResetInterval = m_nLogoInterval * 100;  // Every once in a while refresh the background...

    // Initialize the random number generator
	srand( (unsigned)time( NULL ) );
}

CScrnWnd::~CScrnWnd()
{
}


BEGIN_MESSAGE_MAP(CScrnWnd, CSaverWnd)
	//{{AFX_MSG_MAP(CScrnWnd)
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CScrnWnd message handlers


BOOL CScrnWnd::Create()
{
    if (!CSaverWnd::Create()) 
    {
        return FALSE;
    }
    return TRUE;
}

BOOL CScrnWnd::Create(BOOL bPreview, const RECT& rect, CWnd* pParentWnd)
{
    if (!CSaverWnd::Create(bPreview, rect, pParentWnd)) 
    {
        return FALSE;
    }
    return TRUE;
}

void CScrnWnd::OnTimer(UINT nIDEvent) 
{
    /*
    if (nIDEvent == m_nFadeTimer)
	{
		if(m_nFadeTimer)
			KillTimer(m_nFadeTimer);

        InvalidateRect(m_rectPrevious);
        InvalidateRect(m_rectCurrent);

		m_nFadeTimer = SetTimer(m_nFadeTimer,m_nFadeInterval,NULL);   // Reset the timer
    }
    */

	if (nIDEvent == m_nLogoTimer)
	{
		if(m_nLogoTimer)
			KillTimer(m_nLogoTimer);

        m_rectPrevious = m_rectCurrent;

		CRect rect;
		GetClientRect(&rect);
        int logoWidth =  (m_nLogoWidth * rect.Width()) / ::GetSystemMetrics(SM_CXSCREEN);
        int logoHeight =  (m_nLogoHeight * rect.Height()) / ::GetSystemMetrics(SM_CYSCREEN);

        // Just to be safe,
        // If the image is too big for the screen.....
        logoWidth = (rect.Width() - logoWidth > 0) ? logoWidth : rect.Width() - 1;
        logoHeight = (rect.Height() - logoHeight > 0) ? logoHeight : rect.Height() - 1;

        int x,y;
        if(m_nBackgroundMode == BG_BLACK) 
        {
            x = rand() % (rect.Width() - logoWidth);
            y = rand() % (rect.Height() - logoHeight);
        }
        else
        {
            x = rand() % (rect.Width() + logoWidth) - logoWidth;
            y = rand() % (rect.Height() + logoHeight) - logoHeight;
        }

        m_rectCurrent = CRect(x,y,x+logoWidth,y+logoHeight);
        InvalidateRect(m_rectPrevious);
        InvalidateRect(m_rectCurrent);

		m_nLogoTimer = SetTimer(m_nLogoTimer,m_nLogoInterval,NULL);   // Reset the timer
    }

	if (nIDEvent == m_nResetTimer)
	{
		if(m_nResetTimer)
			KillTimer(m_nResetTimer);

		CRect rect;
		GetClientRect(&rect);
        InvalidateRect(rect);

		m_nResetTimer = SetTimer(m_nResetTimer,m_nResetInterval,NULL);   // Reset the timer
    }

    CSaverWnd::OnTimer(nIDEvent);
}

void CScrnWnd::OnSize(UINT nType, int cx, int cy) 
{
	CSaverWnd::OnSize(nType, cx, cy);

    // Do we need to do anything here?
    // (Screen size can never be changed while the screen saver is running... Right?)
}

void CScrnWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
    dc.SetStretchBltMode(COLORONCOLOR);
    CDC      memDcCurrent;
    CDC      memDcPrevious;
    memDcCurrent.CreateCompatibleDC(&dc);
    memDcPrevious.CreateCompatibleDC(&dc);

    // Paint the background
    DWORD dwRopCurrent;
    DWORD dwRopPrevious;
    if(m_nBackgroundMode == BG_BLACK)
    {
	    CBrush brush(RGB(0,0,0));
	    CRect rect;
	    GetClientRect(rect);
	    dc.FillRect(&rect, &brush);

        if(m_bUseBWLogo)
        {
            memDcCurrent.SelectObject(m_bmReverseLogo);
            memDcPrevious.SelectObject(m_bmReverseLogo);
        }
        else
        {
            memDcCurrent.SelectObject(m_bmLogo);
            memDcPrevious.SelectObject(m_bmLogo);
        }
        dwRopCurrent = SRCCOPY;
        dwRopPrevious = SRCPAINT;

        //memDcCurrent.SelectObject(m_bmLogo);
        //memDcPrevious.SelectObject(m_bmLogo);
        //dwRopCurrent = SRCCOPY;
        //dwRopPrevious = MERGECOPY;
    }
    else
    {
        if(m_bUseBWLogo)
        {
            memDcCurrent.SelectObject(m_bmGrayLogo);
            memDcPrevious.SelectObject(m_bmGrayLogo);
        }
        else
        {
            memDcCurrent.SelectObject(m_bmLogo);
            memDcPrevious.SelectObject(m_bmLogo);
        }
        dwRopCurrent = SRCAND;
        dwRopPrevious = SRCAND;
    }

    // Draw ArinQ Logo
    dc.StretchBlt(m_rectPrevious.left,m_rectPrevious.top,
        m_rectPrevious.right-m_rectPrevious.left,
        m_rectPrevious.bottom-m_rectPrevious.top,
        &memDcPrevious, 0,0, m_nOriginalImageWidth, m_nOriginalImageHeight, dwRopPrevious);

    dc.StretchBlt(m_rectCurrent.left,m_rectCurrent.top,
        m_rectCurrent.right-m_rectCurrent.left,
        m_rectCurrent.bottom-m_rectCurrent.top,
        &memDcCurrent, 0,0, m_nOriginalImageWidth, m_nOriginalImageHeight, dwRopCurrent);

    // Clean up
	DeleteDC(memDcCurrent);
	DeleteDC(memDcPrevious);

	// Do not call CSaverWnd::OnPaint() for painting messages
}

int CScrnWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CSaverWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

    CAIqSaverApp* pApp = (CAIqSaverApp*) AfxGetApp();
    m_strLogoImageName = pApp->GetProfileString(s_profileConfig, s_profileConfigImageName, _T("Default Logo"));
	m_nLogoWidth = pApp->GetProfileInt(s_profileConfig, s_profileConfigLogoWidth, m_nLogoWidth);
	m_nLogoHeight = pApp->GetProfileInt(s_profileConfig, s_profileConfigLogoHeight, m_nLogoHeight);
    m_bUseBWLogo = (BOOL) pApp->GetProfileInt(s_profileConfig, s_profileConfigLogoBWLogo, (int) m_bUseBWLogo);
    m_bUseDefaultAspectRatio = (BOOL) pApp->GetProfileInt(s_profileConfig, s_profileConfigLogoAspectRatio, (int) m_bUseBWLogo);
    m_nBackgroundMode = (BackgroundMode) pApp->GetProfileInt(s_profileConfig, s_profileConfigBackgroundMode, (int) m_nBackgroundMode);

    int nSpeed = pApp->GetProfileInt(s_profileConfig,s_profileConfigAnimationSpeed, 80);
    SetSpeed((nSpeed < 0) ? 0 : nSpeed);

    std::map<CString, CLogoInfo>::iterator it = pApp->m_mapLogoList.find(m_strLogoImageName);
    if(it == pApp->m_mapLogoList.end())
    {
        it = pApp->m_mapLogoList.begin();  // Just use the first element...
    }
    UINT imageID = it->second.GetImageID();
    m_bmLogo.LoadBitmap(imageID);
    m_bmGrayLogo.LoadBitmap(imageID+1);
    m_bmReverseLogo.LoadBitmap(imageID+2);

    m_nOriginalImageWidth = it->second.GetOriginalSizeX();
    m_nOriginalImageHeight = it->second.GetOriginalSizeY();


    // TODO: What if the bitmap loading fails???
    // Just write "Arinquest Technology" on screen....


    CRect rect;
    GetClientRect(&rect);
    int logoWidth = (m_nLogoWidth * rect.Width()) / ::GetSystemMetrics(SM_CXSCREEN);
    int logoHeight = (m_nLogoHeight * rect.Height()) / ::GetSystemMetrics(SM_CYSCREEN);

    // Just to be safe,
    // If the image is too big for the screen.....
    logoWidth = (rect.Width() - logoWidth > 0) ? logoWidth : rect.Width() - 1;
    logoHeight = (rect.Height() - logoHeight > 0) ? logoHeight : rect.Height() - 1;

    // This needs a bit of explanation.
    // If the background is black, then the logos never go out of the screen,
    // whereas when the "transparent" logos are used, the logos should "fill" the whole screen.
    int xp,yp,xc,yc;
    if(m_nBackgroundMode == BG_BLACK) 
    {
        xp = rand() % (rect.Width() - logoWidth);
        yp = rand() % (rect.Height() - logoHeight);
        xc = rand() % (rect.Width() - logoWidth);
        yc = rand() % (rect.Height() - logoHeight);
    }
    else
    {
        xp = rand() % (rect.Width() + logoWidth) - logoWidth;
        yp = rand() % (rect.Height() + logoHeight) - logoHeight;
        xc = rand() % (rect.Width() + logoWidth) - logoWidth;
        yc = rand() % (rect.Height() + logoHeight) - logoHeight;
    }
    m_rectPrevious = CRect(xp,yp,xp+logoWidth,yp+logoHeight);
    m_rectCurrent = CRect(xc,yc,xc+logoWidth,yc+logoHeight);

	return 0;
}

void CScrnWnd::OnDestroy() 
{
	CSaverWnd::OnDestroy();
	
    DeleteObject(m_bmLogo);
    DeleteObject(m_bmGrayLogo);
    DeleteObject(m_bmReverseLogo);

    if(m_nFadeTimer)
        KillTimer(m_nFadeTimer);
    if(m_nLogoTimer)
        KillTimer(m_nLogoTimer);
    if(m_nResetTimer)
        KillTimer(m_nResetTimer);
}

void CScrnWnd::SetSpeed(int nSpeed)
{
    ResetTimer(nSpeed);
}

void CScrnWnd::ResetTimer(int nSpeed)
{
    ///// nSpeed varis from 1 (slow) to 100 (fast).
    ///// The interval should be somewhere in the range of 10~100 seconds to 50~100 milli seconds.
    ///// Also, the middle point (nSpeed==50) should roughly correspond to a reasoably comfortable speed.
    //m_nLogoInterval = (100-nSpeed)*100 + 50;
    //m_nLogoInterval = 10000 / (nSpeed + 1);
    m_nLogoInterval = (int) sqrt((double) ((100-nSpeed)*750 + 1) * (10000 / (nSpeed + 1))) + 50;
    /////

    m_nFadeInterval = m_nLogoInterval / 10;
    m_nResetInterval = m_nLogoInterval * 100;

    if(m_nFadeTimer)
        KillTimer(m_nFadeTimer);
    if(m_nLogoTimer)
        KillTimer(m_nLogoTimer);
    if(m_nResetTimer)
        KillTimer(m_nResetTimer);

	m_nFadeTimer = SetTimer(2121,m_nFadeInterval,NULL);
	m_nLogoTimer = SetTimer(2222,m_nLogoInterval,NULL);
	m_nResetTimer = SetTimer(2323,m_nResetInterval,NULL);
}

