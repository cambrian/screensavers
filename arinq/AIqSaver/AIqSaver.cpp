////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////
//
// AIqSaver.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "AIqSaver.h"
#include "ConfigDlg.h"
#include "LogoWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CAIqSaverApp

BEGIN_MESSAGE_MAP(CAIqSaverApp, CWinApp)
	//{{AFX_MSG_MAP(CAIqSaverApp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAIqSaverApp construction

CAIqSaverApp::CAIqSaverApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance

    m_mapLogoList = std::map<CString, CLogoInfo>();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CAIqSaverApp object

CAIqSaverApp theApp;


/////////////////////////////////////////////////////////////////////////////
// Convenience function to parse command line options.

BOOL MatchOption(LPTSTR lpsz, LPTSTR lpszOption)
{
	if (lpsz[0] == '-' || lpsz[0] == '/')
		lpsz++;
	if (lstrcmpi(lpsz, lpszOption) == 0)
		return TRUE;
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CAIqSaverApp initialization

BOOL CAIqSaverApp::InitInstance()
{
	SetRegistryKey(s_profileCompany);

    // Construct logo list here....
    CLogoInfo logoInfo0(_T("Default Logo"), IDB_STANDARDLOGO, 200, 120, 200, 120);
    CLogoInfo logoInfo1(_T("Fancy ArinQ Logo"), IDB_FANCYLOGO, 200, 120, 200, 120);
    m_mapLogoList.insert(std::make_pair(logoInfo0.GetName(), logoInfo0));
    m_mapLogoList.insert(std::make_pair(logoInfo1.GetName(), logoInfo1));

    ////////////////////////////////////////////////////////////
    // This section is what makes screen savers screen savers!
    ////////////////////////////////////////////////////////////
    // If the command line contains /s -s or s then just run as a 
    // screen saver.  If the command line is blank or contains 
    // /c -c or c then show the config dialog
    if (__argc == 1)
    {
        // Run the config dlg with no parent
        DoConfig();
    }
	else if (MatchOption(__argv[1], _T("s")))
	{
        // Run as screen saver
        CLogoWnd* pWnd = new CLogoWnd();
        pWnd->Create();
        m_pMainWnd = pWnd;
	    return TRUE;
	}
	else if (MatchOption(__argv[1], _T("p")))
	{
        // Run as a subwindow...
		CWnd* pParent = CWnd::FromHandle((HWND)atol(__argv[2]));
		ASSERT(pParent != NULL);
		CLogoWnd* pWnd = new CLogoWnd();
		CRect rect;
		pParent->GetClientRect(&rect);
        pWnd->Create(TRUE, rect, pParent);
		m_pMainWnd = pWnd;
		return TRUE;
	}
    else 
    {
        // Run the configuration dialog
        if (MatchOption(__argv[1], _T("c")))
        {
            // Run config with current window as parent
            DoConfig(CWnd::GetActiveWindow());
        } else {
            // Run the config dlg with no parent
            DoConfig();
        }
    }
    ////////////////////////////////////////////////////////////
    // This section is what makes screen savers screen savers!
    ////////////////////////////////////////////////////////////

    // Now just terminate
    return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// Open the "Settings..." window.

void CAIqSaverApp::DoConfig(CWnd* pParent)
{
    CConfigDlg dlg(pParent);

    // Set up the current params
	dlg.m_strLogoImageName = GetProfileString(s_profileConfig, s_profileConfigImageName, _T("Default Logo"));
	dlg.m_bUseBWLogo = (BOOL) GetProfileInt(s_profileConfig, s_profileConfigLogoBWLogo, 0);
	dlg.m_nLogoSizeX = GetProfileInt(s_profileConfig, s_profileConfigLogoWidth, 220);
	dlg.m_nLogoSizeY = GetProfileInt(s_profileConfig, s_profileConfigLogoHeight, 160);
	dlg.m_bUseDefaultAspectRatio = (BOOL) GetProfileInt(s_profileConfig, s_profileConfigLogoAspectRatio, 1);
    dlg.m_nBackground = GetProfileInt(s_profileConfig, s_profileConfigBackgroundMode, (int) BG_BLACK);
	dlg.m_nSpeed = GetProfileInt(s_profileConfig, s_profileConfigAnimationSpeed, 50);

    // OK, I'm actually the only window in the current context.
    m_pMainWnd = &dlg;

    // Do the dialog,
    // and save the result in the registry.
    if (dlg.DoModal() == IDOK)
    {
		WriteProfileString(s_profileConfig, s_profileConfigImageName, dlg.m_strLogoImageName);
		WriteProfileInt(s_profileConfig, s_profileConfigAnimationSpeed, dlg.m_nSpeed);
		WriteProfileInt(s_profileConfig, s_profileConfigLogoBWLogo, (int) dlg.m_bUseBWLogo);
		WriteProfileInt(s_profileConfig, s_profileConfigLogoWidth, dlg.m_nLogoSizeX);
		WriteProfileInt(s_profileConfig, s_profileConfigLogoHeight, dlg.m_nLogoSizeY);
		WriteProfileInt(s_profileConfig, s_profileConfigLogoAspectRatio, (int) dlg.m_bUseDefaultAspectRatio);
        WriteProfileInt(s_profileConfig, s_profileConfigBackgroundMode, (int) dlg.m_nBackground);
	}
}


/////////////////////////////////////////////////////////////////////////////
// CAIqSaverApp message handlers


