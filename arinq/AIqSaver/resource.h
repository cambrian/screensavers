//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AIqSaver.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDC_NULLCURSOR                  130
#define IDD_CONFIG_DIALOG               131
#define IDB_FANCYLOGO                   143
#define IDB_FANCYLOGO_GRAY              144
#define IDB_FANCYLOGO_REVERSE           145
#define IDB_STANDARDLOGO                146
#define IDB_STANDARDLOGO_GRAY           147
#define IDB_STANDARDLOGO_REVERSE        148
#define IDC_BACK_SCREEN                 1000
#define IDC_BACK_BLACK                  1001
#define IDC_LOGO_SIZEX                  1002
#define IDC_LOGO_SIZEY                  1003
#define IDC_LOGO_COLOR_PREVIEW          1004
#define IDC_LOGO_COLOR_CHANGE           1005
#define IDC_PREVIEW                     1006
#define IDC_SPEED_BAR                   1007
#define IDC_ABOUT                       1009
#define IDC_USE_BWLOGO                  1010
#define IDC_LOGO_IMAGE                  1011
#define IDC_USE_DEFRATIO                1012
#define IDC_BUTTON1                     1013
#define IDC_RESET_SIZE                  1013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
