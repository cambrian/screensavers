////////////////////////////////////////////////////////////////////////
// ArinQ-Logo Screen Saver.
// Copyright (C) 2005, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of Arinquest Technology.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@arinquest.com.
////////////////////////////////////////////////////////////////////////
//
// AIqSaver.h : main header file for the PEOPLESAVER application
//

#if !defined(AFX_PEOPLESAVER_H__668F7D16_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
#define AFX_PEOPLESAVER_H__668F7D16_D6DF_11D5_8DA8_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <map>
#include "resource.h"       // main symbols
#include "LogoInfo.h"


////// Strings for registry entry
static const TCHAR s_profileCompany[] = _T("HYoon Screen Savers");

// Profile Section
static const TCHAR s_profileConfig[] = _T("Settings");
static const TCHAR s_profileConfigImageName[] = _T("Logo Image Name");
static const TCHAR s_profileConfigBackgroundMode[] = _T("Background Mode");
static const TCHAR s_profileConfigAnimationSpeed[] = _T("Animation Speed");
static const TCHAR s_profileConfigLogoBWLogo[] = _T("Logo Use BWLogo");
static const TCHAR s_profileConfigLogoWidth[] = _T("Logo Width");
static const TCHAR s_profileConfigLogoHeight[] = _T("Logo Height");
static const TCHAR s_profileConfigLogoAspectRatio[] = _T("Logo Use DefaultRatio");


/////////////////////////////////////////////////////////////////////////////
// CAIqSaverApp:
// See AIqSaver.cpp for the implementation of this class
//

class CAIqSaverApp : public CWinApp
{
public:
	CAIqSaverApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAIqSaverApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

private:
    void DoConfig(CWnd* pParent = NULL);

//private:
public:
    std::map<CString, CLogoInfo>   m_mapLogoList;

public:
	//{{AFX_MSG(CAIqSaverApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PEOPLESAVER_H__668F7D16_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
