////////////////////////////////////////////////////////////////////////
// Company-Logo Screen Saver.
// Copyright (C) 2001-2004, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of respective companies.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@computer.org
////////////////////////////////////////////////////////////////////////
//
// ScrnWnd.cpp : implementation file
//

#include "stdafx.h"
#include "LogoSaver.h"
#include "ScrnWnd.h"
#include <stdlib.h>
#include <time.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CScrnWnd

CScrnWnd::CScrnWnd()
{
    m_nBackgroundMode = BG_BLACK;
    m_nLogoWidth = 2*CompanyLogoDefaultWidth;
    m_nLogoHeight = 2*CompanyLogoDefaultHeight;
    m_nBackgroundMode = BG_BLACK;
    m_bUseBWLogo = TRUE;

    m_nLogoTimer = 0;
    m_nFadeTimer = 0;
    m_nResetTimer = 0;
    m_nLogoInterval = 1 * 1000;
    m_nFadeInterval = m_nLogoInterval / 10;
    m_nResetInterval = m_nLogoInterval * 100;

    // Initialize the random number generator
	srand( (unsigned)time( NULL ) );

}

CScrnWnd::~CScrnWnd()
{
}


BEGIN_MESSAGE_MAP(CScrnWnd, CSaverWnd)
	//{{AFX_MSG_MAP(CScrnWnd)
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CScrnWnd message handlers


BOOL CScrnWnd::Create()
{
    if (!CSaverWnd::Create()) 
    {
        return FALSE;
    }
    return TRUE;
}

BOOL CScrnWnd::Create(BOOL bPreview, const RECT& rect, CWnd* pParentWnd)
{
    if (!CSaverWnd::Create(bPreview, rect, pParentWnd)) 
    {
        return FALSE;
    }
    return TRUE;
}

void CScrnWnd::OnTimer(UINT nIDEvent) 
{
    /*
    if (nIDEvent == m_nFadeTimer)
	{
		if(m_nFadeTimer)
			KillTimer(m_nFadeTimer);

        InvalidateRect(m_rectPrevious);
        InvalidateRect(m_rectCurrent);

		m_nFadeTimer = SetTimer(m_nFadeTimer,m_nFadeInterval,NULL);   // Reset the timer
    }
    */
	
	if (nIDEvent == m_nLogoTimer)
	{
		if(m_nLogoTimer)
			KillTimer(m_nLogoTimer);

        m_rectPrevious = m_rectCurrent;

		CRect rect;
		GetClientRect(&rect);
        int logoWidth =  (m_nLogoWidth * rect.Width()) / ::GetSystemMetrics(SM_CXSCREEN);
        int logoHeight =  (m_nLogoHeight * rect.Height()) / ::GetSystemMetrics(SM_CYSCREEN);

        int x,y;
        if(m_nBackgroundMode == BG_BLACK) 
        {
            x = rand() % (rect.Width() - logoWidth);
            y = rand() % (rect.Height() - logoHeight);
        }
        else
        {
            x = rand() % (rect.Width() + logoWidth) - logoWidth;
            y = rand() % (rect.Height() + logoHeight) - logoHeight;
        }

        m_rectCurrent = CRect(x,y,x+logoWidth,y+logoHeight);
        InvalidateRect(m_rectPrevious);
        InvalidateRect(m_rectCurrent);

		m_nLogoTimer = SetTimer(m_nLogoTimer,m_nLogoInterval,NULL);   // Reset the timer
    }

	if (nIDEvent == m_nResetTimer)
	{
		if(m_nResetTimer)
			KillTimer(m_nResetTimer);

		CRect rect;
		GetClientRect(&rect);
        InvalidateRect(rect);

		m_nResetTimer = SetTimer(m_nResetTimer,m_nResetInterval,NULL);   // Reset the timer
    }

    CSaverWnd::OnTimer(nIDEvent);
}

void CScrnWnd::OnSize(UINT nType, int cx, int cy) 
{
	CSaverWnd::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}

void CScrnWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
    dc.SetStretchBltMode(COLORONCOLOR);
    CDC      memDcCurrent;
    CDC      memDcPrevious;
    memDcCurrent.CreateCompatibleDC(&dc);
    memDcPrevious.CreateCompatibleDC(&dc);


    // Paint the background
    DWORD dwRopCurrent;
    DWORD dwRopPrevious;
    if(m_nBackgroundMode == BG_BLACK)
    {
	    CBrush brush(RGB(0,0,0));
	    CRect rect;
	    GetClientRect(rect);
	    dc.FillRect(&rect, &brush);

        if(m_bUseBWLogo)
        {
            memDcCurrent.SelectObject(m_bmReverseLogo);
            memDcPrevious.SelectObject(m_bmReverseLogo);
        }
        else
        {
            memDcCurrent.SelectObject(m_bmLogo);
            memDcPrevious.SelectObject(m_bmLogo);
        }
        dwRopCurrent = SRCCOPY;
        dwRopPrevious = SRCPAINT;

        //memDcCurrent.SelectObject(m_bmLogo);
        //memDcPrevious.SelectObject(m_bmLogo);
        //dwRopCurrent = SRCCOPY;
        //dwRopPrevious = MERGECOPY;
    }
    else
    {
        if(m_bUseBWLogo)
        {
            memDcCurrent.SelectObject(m_bmGrayLogo);
            memDcPrevious.SelectObject(m_bmGrayLogo);
        }
        else
        {
            memDcCurrent.SelectObject(m_bmLogo);
            memDcPrevious.SelectObject(m_bmLogo);
        }
        dwRopCurrent = SRCAND;
        dwRopPrevious = SRCAND;
    }


    // Draw Company Logo
    dc.StretchBlt(m_rectPrevious.left,m_rectPrevious.top,
        m_rectPrevious.right-m_rectPrevious.left,
        m_rectPrevious.bottom-m_rectPrevious.top,
        &memDcPrevious, 0,0,CompanyLogoDefaultWidth,CompanyLogoDefaultHeight, dwRopPrevious);

    dc.StretchBlt(m_rectCurrent.left,m_rectCurrent.top,
        m_rectCurrent.right-m_rectCurrent.left,
        m_rectCurrent.bottom-m_rectCurrent.top,
        &memDcCurrent, 0,0,CompanyLogoDefaultWidth,CompanyLogoDefaultHeight, dwRopCurrent);
    

    // Clean up
	DeleteDC(memDcCurrent);
	DeleteDC(memDcPrevious);


	// Do not call CSaverWnd::OnPaint() for painting messages
}

int CScrnWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CSaverWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

    m_bmLogo.LoadBitmap(IDB_BRIOLOGO);
    m_bmGrayLogo.LoadBitmap(IDB_GRAYLOGO);
    m_bmDarkLogo.LoadBitmap(IDB_DARKLOGO);
    m_bmReverseLogo.LoadBitmap(IDB_REVERSELOGO);
	

    CWinApp* pApp = AfxGetApp();
	//m_colorLogo = (COLORREF) pApp->GetProfileInt(s_profileConfig, s_profileConfigLogoColor, 0xFFFFFF);
	m_nLogoWidth = pApp->GetProfileInt(s_profileConfig, s_profileConfigLogoWidth, m_nLogoWidth);
	m_nLogoHeight = pApp->GetProfileInt(s_profileConfig, s_profileConfigLogoHeight, m_nLogoHeight);
    m_nBackgroundMode = (BackgroundMode) pApp->GetProfileInt(s_profileConfig, s_profileConfigBackgroundMode, (int) m_nBackgroundMode);
    m_bUseBWLogo = (BOOL) pApp->GetProfileInt(s_profileConfig, s_profileConfigLogoBWLogo, (int) m_bUseBWLogo);

    int nSpeed = pApp->GetProfileInt(s_profileConfig,s_profileConfigAnimationSpeed, 80);
	if (nSpeed < 0)
		nSpeed = 0;
	SetSpeed(nSpeed);


    CRect rect;
    GetClientRect(&rect);
    int logoWidth =  (m_nLogoWidth * rect.Width()) / ::GetSystemMetrics(SM_CXSCREEN);
    int logoHeight =  (m_nLogoHeight * rect.Height()) / ::GetSystemMetrics(SM_CYSCREEN);

    int xp,yp,xc,yc;
    if(m_nBackgroundMode == BG_BLACK) 
    {
        xp = rand() % (rect.Width() - logoWidth);
        yp = rand() % (rect.Height() - logoHeight);
        xc = rand() % (rect.Width() - logoWidth);
        yc = rand() % (rect.Height() - logoHeight);
    }
    else
    {
        xp = rand() % (rect.Width() + logoWidth) - logoWidth;
        yp = rand() % (rect.Height() + logoHeight) - logoHeight;
        xc = rand() % (rect.Width() + logoWidth) - logoWidth;
        yc = rand() % (rect.Height() + logoHeight) - logoHeight;
    }
    m_rectPrevious = CRect(xp,yp,xp+logoWidth,yp+logoHeight);
    m_rectCurrent = CRect(xc,yc,xc+logoWidth,yc+logoHeight);

	return 0;
}



void CScrnWnd::OnDestroy() 
{
	CSaverWnd::OnDestroy();
	
    DeleteObject(m_bmLogo);
    DeleteObject(m_bmGrayLogo);
    DeleteObject(m_bmDarkLogo);
    DeleteObject(m_bmReverseLogo);

    if(m_nFadeTimer)
        KillTimer(m_nFadeTimer);
    if(m_nLogoTimer)
        KillTimer(m_nLogoTimer);
    if(m_nResetTimer)
        KillTimer(m_nResetTimer);
}



void CScrnWnd::SetSpeed(int nSpeed)
{
    ResetTimer(nSpeed);
}

void CScrnWnd::ResetTimer(int nSpeed)
{
    m_nLogoInterval = (100-nSpeed)*100 + 50;
    m_nFadeInterval = m_nLogoInterval / 10;
    m_nResetInterval = m_nLogoInterval * 100;

    if(m_nFadeTimer)
        KillTimer(m_nFadeTimer);
    if(m_nLogoTimer)
        KillTimer(m_nLogoTimer);
    if(m_nResetTimer)
        KillTimer(m_nResetTimer);

	m_nFadeTimer = SetTimer(2121,m_nFadeInterval,NULL);
	m_nLogoTimer = SetTimer(2222,m_nLogoInterval,NULL);
	m_nResetTimer = SetTimer(2323,m_nResetInterval,NULL);

}

