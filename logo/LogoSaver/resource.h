//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by LogoSaver.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_BRIOLOTYPE                  129
#define IDC_NULLCURSOR                  130
#define IDD_CONFIG_DIALOG               131
#define IDB_BRIOLOGO                    132
#define IDB_GRAYMASK                    135
#define IDB_DARKLOGO                    136
#define IDB_REVERSELOGO                 137
#define IDB_GRAYLOGO                    138
#define IDC_BACK_SCREEN                 1000
#define IDC_BACK_BLACK                  1001
#define IDC_LOGO_SIZEX                  1002
#define IDC_LOGO_SIZEY                  1003
#define IDC_LOGO_COLOR_PREVIEW          1004
#define IDC_LOGO_COLOR_CHANGE           1005
#define IDC_PREVIEW                     1006
#define IDC_SPEED_BAR                   1007
#define IDC_ABOUT                       1009
#define IDC_USE_BWLOGO                  1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
