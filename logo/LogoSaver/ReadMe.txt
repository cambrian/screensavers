[LogoSaver Screen Saver]

CLogoSaverApp: Main Driver class
CSaverWnd: This class implements essential functions of Screen saver.
CScrnWnd: This class provides basic drawing routines imitating "Windows logon screen saver".
CLogoWnd: Simple subclass of CScrnWnd. (Currently, this class doesn't introduce any new functionalities over CScrnWnd.)
CConfigDlg: Dialog window to be used for "Setting..." purporses.


