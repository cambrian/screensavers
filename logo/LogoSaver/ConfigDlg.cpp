////////////////////////////////////////////////////////////////////////
// Company-Logo Screen Saver.
// Copyright (C) 2001-2004, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of respective companies.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@computer.org
////////////////////////////////////////////////////////////////////////
//
// ConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LogoSaver.h"
#include "ConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()




/////////////////////////////////////////////////////////////////////////////
// CConfigDlg dialog

CConfigDlg::CConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDlg::IDD, pParent), m_wndPreview()
{
	//{{AFX_DATA_INIT(CConfigDlg)
	m_nLogoSizeX = 0;
	m_nLogoSizeY = 0;
	m_nSpeed = 0;
	m_nBackground = -1;
	m_bUseBWLogo = FALSE;
	//}}AFX_DATA_INIT

    //m_nBackgroundMode = BG_BLACK;	
    m_colorLogo = PALETTERGB(0,0,0);

   	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

}


void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigDlg)
	DDX_Control(pDX, IDC_SPEED_BAR, m_scrollSpeed);
	DDX_Text(pDX, IDC_LOGO_SIZEX, m_nLogoSizeX);
	DDV_MinMaxInt(pDX, m_nLogoSizeX, 25, 500);
	DDX_Text(pDX, IDC_LOGO_SIZEY, m_nLogoSizeY);
	DDV_MinMaxInt(pDX, m_nLogoSizeY, 15, 300);
	DDX_Scroll(pDX, IDC_SPEED_BAR, m_nSpeed);
	DDX_Radio(pDX, IDC_BACK_BLACK, m_nBackground);
	DDX_Check(pDX, IDC_USE_BWLOGO, m_bUseBWLogo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CConfigDlg)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BACK_BLACK, OnBackBlack)
	ON_BN_CLICKED(IDC_BACK_SCREEN, OnBackScreen)
	ON_BN_CLICKED(IDC_LOGO_COLOR_CHANGE, OnLogoColorChange)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_ABOUT, OnAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg message handlers

BOOL CConfigDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CRect rect;
	GetDlgItem(IDC_PREVIEW)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	m_wndPreview.Create(TRUE, rect, this);

	m_scrollSpeed.SetScrollRange(1, 100);
	m_scrollSpeed.SetScrollPos(m_nSpeed);

	CenterWindow();

    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon


    return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CConfigDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

void CConfigDlg::OnBackBlack() 
{
    //m_nBackgroundMode = BG_BLACK;	
    //m_nBackground = 0;
}

void CConfigDlg::OnBackScreen() 
{
    //m_nBackgroundMode = BG_SCREEN;	
    //m_nBackground = 1;
}

void CConfigDlg::OnLogoColorChange() 
{
	// TODO:
	AfxMessageBox(_T("Not Implemented."));
}

void CConfigDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	ASSERT(pScrollBar != NULL);
	int nCurPos = pScrollBar->GetScrollPos();
	switch (nSBCode)
	{
	case SB_LEFT: //    Scroll to far left.
		nCurPos = 1;
		break;
	case SB_LINELEFT: //    Scroll left.
		nCurPos--;
		break;
	case SB_LINERIGHT: //    Scroll right.
		nCurPos++;
		break;
	case SB_PAGELEFT: //    Scroll one page left.
		nCurPos -= 10;
		break;
	case SB_PAGERIGHT: //    Scroll one page right.
		nCurPos += 10;
		break;
	case SB_RIGHT: //    Scroll to far right.
		nCurPos = 100;
		break;
	case SB_THUMBPOSITION: //    Scroll to absolute position. The current position is specified by the nPos parameter.
	case SB_THUMBTRACK: //    Drag scroll box to specified position. The current position is specified by the nPos parameter.
		nCurPos = nPos;
	}
	if (nCurPos < 1)
		nCurPos = 1;
	if (nCurPos > 100)
		nCurPos = 100;
	pScrollBar->SetScrollPos(nCurPos);

	m_wndPreview.SetSpeed(m_scrollSpeed.GetScrollPos());

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}



void CConfigDlg::OnAbout() 
{
    // App command to run the dialog
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

