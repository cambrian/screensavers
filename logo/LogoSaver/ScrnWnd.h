////////////////////////////////////////////////////////////////////////
// Company-Logo Screen Saver.
// Copyright (C) 2001-2004, Hyoungsoo Yoon.
// (The logos used in this program are trademarks of respective companies.)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// You can contact the author by electronic mail at hyoon@computer.org
////////////////////////////////////////////////////////////////////////
//
// ScrnWnd.h : header file
//
#if !defined(AFX_SCRNWND_H__668F7D24_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
#define AFX_SCRNWND_H__668F7D24_D6DF_11D5_8DA8_00207817FF60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "SaverWnd.h"

enum  BackgroundMode {BG_BLACK=0, BG_SCREEN=1}; 
const int   CompanyLogoDefaultWidth = 158;
const int   CompanyLogoDefaultHeight = 95;


/////////////////////////////////////////////////////////////////////////////
// CScrnWnd window
// This class implements basic drawing logics of screen savers.

class CScrnWnd : public CSaverWnd
{
// Construction
public:
	CScrnWnd();
    BOOL Create();
    BOOL Create(BOOL bPreview, const RECT& rect, CWnd* pParentWnd);

// Attributes
public:

private:
	CBitmap  m_bmLogo;
 	CBitmap  m_bmGrayLogo;
 	CBitmap  m_bmDarkLogo;
 	CBitmap  m_bmReverseLogo;
    int      m_nLogoWidth;
    int      m_nLogoHeight;
    BackgroundMode  m_nBackgroundMode;
    BOOL     m_bUseBWLogo;

	UINT     m_nFadeTimer;
	UINT     m_nLogoTimer;
	UINT     m_nResetTimer;
    int      m_nFadeInterval;
    int      m_nLogoInterval;
    int      m_nResetInterval;
    CRect    m_rectPrevious;
    CRect    m_rectCurrent;


// Operations
public:
	void SetSpeed(int nSpeed);

private:
    void ResetTimer(int nSpeed);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScrnWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CScrnWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CScrnWnd)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCRNWND_H__668F7D24_D6DF_11D5_8DA8_00207817FF60__INCLUDED_)
